﻿<%@ Page Title="Registration" Language="VB" AutoEventWireup="false" CodeFile="Registration.aspx.vb" Inherits="Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Warwick Social Network</title>

    <!-- Browser icon -->
    <link rel="shortcut icon" href="Pictures/favicon.ico">

    <!-- Bootstrap -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/login.css" type="text/css">
<<<<<<< HEAD
   
=======

    <!--ListBox functionality: enables multiple selection. DON'T HOLD "ctrl" WHEN SELECTING! WON'T WORK-->
    <!--Source: https://stackoverflow.com/questions/33523347/how-to-multiple-select-item-in-listbox-without-press-ctrl-in-asp-net-->
    <script type="text/javascript" language="javascript">
        var selectedClientPermissions = [];

        function pageLoad() {
            var ListBox1 = document.getElementById("<%= ListBoxInterests.ClientID %>");

            for (var i = 0; i < ListBox1.length; i++) {
                selectedClientPermissions[i] = ListBox1.options[i].selected;
            }
        }

        function ListBoxClient_SelectionChanged(sender, args) {
            var scrollPosition = sender.scrollTop;

            for (var i = 0; i < sender.length; i++) {
                if (sender.options[i].selected) selectedClientPermissions[i] = !selectedClientPermissions[i];

                sender.options[i].selected = selectedClientPermissions[i] === true;
            }

            sender.scrollTop = scrollPosition;
        }
    </script>

>>>>>>> 51d70a2b95253a161c643f1cbe05527a90414bb2
</head>
<body>

    <!-- Top nav -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a class="navbar-brand" href="Login.aspx">We@Warwick</a></li>
            </ul>
        </div>
    </nav>
    <!-- Nav End -->

    <div class="hero-image">
        <!-- Background picture -->
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <asp:HyperLink ID="HyperLinkLogin" runat="server" NavigateUrl="~/Login.aspx">Login</asp:HyperLink>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" class="active" id="register-form-link">Registration</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="form1" runat="server">
                                        <div class="form-group">
                                            <asp:TextBox ID="FirstName" runat="server" class="form-control" placeholder="First Name"></asp:TextBox>

                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="LastName" runat="server" class="form-control" placeholder="Last Name"></asp:TextBox>

                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="Email" runat="server" class="form-control" placeholder="Your Warwick Email Address"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <asp:DropDownList ID="DropDownListCourse" runat="server" class="form-control" DataSourceID="AccessDataSourceCourse" DataTextField="course_name" DataValueField="ID">
                                            </asp:DropDownList>
<<<<<<< HEAD
                                            <asp:AccessDataSource ID="AccessDataSourceCourse" runat="server" SelectCommand="SELECT [course_name], [ID] FROM [courses] ORDER BY [ID] DESC" DataFile="~/App_Data/1419631.mdb"></asp:AccessDataSource>                                            
=======
                                            <asp:AccessDataSource ID="AccessDataSourceCourse" runat="server" SelectCommand="SELECT [course_name], [ID] FROM [courses] ORDER BY [ID] DESC" DataFile="~/App_Data/WeAtWarwick.mdb"></asp:AccessDataSource>                                            
>>>>>>> 51d70a2b95253a161c643f1cbe05527a90414bb2
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="TextBoxCalendar" runat="server" class="form-control" placeholder="dd/mm/yyyy" Type="date"></asp:TextBox>
                                        </div>

                                        <div class="form-group">
                                            <p>What are you interested in?</p>
                                            <asp:ListBox ID="ListBoxInterests" class="form-control" runat="server" DataSourceID="AccessDataSourceInterests" DataTextField="interests" DataValueField="interests" SelectionMode="Multiple" onclick="ListBoxClient_SelectionChanged(this, event);"></asp:ListBox>
<<<<<<< HEAD
                                            <asp:AccessDataSource ID="AccessDataSourceInterests" runat="server" SelectCommand="SELECT [interests] FROM [interests]" DataFile="~/App_Data/1419631.mdb"></asp:AccessDataSource>                                            
=======
                                            <asp:AccessDataSource ID="AccessDataSourceInterests" runat="server" SelectCommand="SELECT [interests] FROM [interests]" DataFile="~/App_Data/WeAtWarwick.mdb"></asp:AccessDataSource>                                            
>>>>>>> 51d70a2b95253a161c643f1cbe05527a90414bb2
                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="Password" runat="server" TextMode="Password" class="form-control" placeholder="Password: at least 8 characters long"></asp:TextBox>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <asp:Button ID="Submit" runat="server" Text="Register" OnClick="Submit_Click" class="form-control btn btn-register" />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
