﻿<%@ Page Title="Network" Language="VB" AutoEventWireup="false" MasterPageFile="~/Master.Master" CodeFile="Network.aspx.vb" Inherits="Network" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styling.css" type="text/css">

    <!--JQuery Dialog http://jqueryui.com/dialog/ Used for "Detail View" -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>           
        //Show/hide list of all users
        function changeVisibility(link) {
            if (document.getElementById("ButtonVisibility").innerHTML == "Show all users") {
                document.getElementById("ButtonVisibility").innerHTML = "Show users with selected interests";
                document.getElementById("UsersInterest").classList.remove("visible");
                document.getElementById("UsersInterest").classList.add("hidden");
                document.getElementById("UsersAll").classList.remove("hidden");
                document.getElementById("UsersAll").classList.add("visible");
            }
            else {
                document.getElementById("ButtonVisibility").innerHTML = "Show all users";
                document.getElementById("UsersInterest").classList.remove("hidden");
                document.getElementById("UsersInterest").classList.add("visible");
                document.getElementById("UsersAll").classList.remove("visible");
                document.getElementById("UsersAll").classList.add("hidden");
            }
        }
        //Hide detail view
        function hideDetail(link) {
            document.getElementById("detail").classList.add("hidden");
        }
    </script>

<<<<<<< HEAD
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h1 style="margin-left: 5px; text-align: center;">Network</h1>                
=======
    <!--Enable multiple selection from listbox without holding "ctrl"-->
     <script type="text/javascript" language="javascript">
         var selectedClientPermissions = [];

         function pageLoad() {
             var ListBox1 = document.getElementById("<%= ListBoxSearch.ClientID %>");

             for (var i = 0; i < ListBox1.length; i++) {
                 selectedClientPermissions[i] = ListBox1.options[i].selected;
             }
         }

         function ListBoxClient_SelectionChanged(sender, args) {
             var scrollPosition = sender.scrollTop;

             for (var i = 0; i < sender.length; i++) {
                 if (sender.options[i].selected) selectedClientPermissions[i] = !selectedClientPermissions[i];

                 sender.options[i].selected = selectedClientPermissions[i] === true;
             }

             sender.scrollTop = scrollPosition;
         }
    </script>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h1 style="margin-left: 5px; text-align: center">Network</h1>
>>>>>>> 51d70a2b95253a161c643f1cbe05527a90414bb2
            </div>
            <!--SEARCH BY INTEREST-->
            <div class="col-xs-12 col-sm-12 col-md-6 form-group">
                <h3 style="margin-left: 5px; text-align: center">Find users with specific interests!</h3>

<<<<<<< HEAD
                <asp:AccessDataSource ID="AccessDataSourceSearch" runat="server" SelectCommand="SELECT * FROM [interests]" DataFile="~/App_Data/1419631.mdb"></asp:AccessDataSource>
                <asp:ListBox ID="ListBoxSearch" class="form-control" runat="server" DataSourceID="AccessDataSourceSearch" DataTextField="interests" DataValueField="ID" SelectionMode="Multiple" onclick="ListBoxClient_SelectionChanged(this, event);" Rows="10"></asp:ListBox>
=======
                <asp:AccessDataSource ID="AccessDataSourceSearch" runat="server" SelectCommand="SELECT * FROM [interests]" DataFile="~/App_Data/WeAtWarwick.mdb"></asp:AccessDataSource>
                <asp:ListBox ID="ListBoxSearch" class="form-control" runat="server" DataSourceID="AccessDataSourceSearch" DataTextField="interests" DataValueField="ID" SelectionMode="Multiple" onclick="ListBoxClient_SelectionChanged(this, event);"></asp:ListBox>
>>>>>>> 51d70a2b95253a161c643f1cbe05527a90414bb2

                <asp:Button ID="ButtonSubmit" runat="server" Text="Search for users by interest" class="btn btn-default col-xs-12 col-sm-12 col-md-6" Style="margin-top: 5px;" />
                <button type="button" id="ButtonVisibility" onclick="changeVisibility(this); return false;" class="btn btn-default col-xs-12 col-sm-12 col-md-6" style="margin-top: 5px;">Show all users</button>

                <div id="UsersInterest" class="visible">
<<<<<<< HEAD
                    <asp:Panel ID="PanelInterestFriends" style="margin-left:70px; padding-top:50px;" runat="server"></asp:Panel>                    
                </div>

                <div id="UsersAll" class="hidden">
                    <asp:Panel ID="PanelUsers" runat="server" style="margin-left:70px; padding-top:50px;"></asp:Panel>                    
                </div>
                <asp:Panel ID="PanelDetail" runat="server" Style="padding-left: 180px;"></asp:Panel>
            </div>

            <!--DETAIL VIEW and RECOMMENDED FRIENDS-->
            <div class="col-xs-12 col-sm-12 col-md-6 form-group">                
                <h3 style="margin-left: 5px; text-align: center">Users with similar interests to you</h3>
                <asp:Panel ID="PanelRecommendedFriends" runat="server" style="margin-left:130px; padding-top:20px;"></asp:Panel>
                
=======
                    <asp:Panel ID="PanelInterestFriends" Style="margin-left: -50px;" runat="server"></asp:Panel>
                </div>

                <div id="UsersAll" class="hidden">
                    <asp:Panel ID="PanelUsers" runat="server"></asp:Panel>
                </div>
            </div>

            <!--DETAIL VIEW and RECOMMENDED FRIENDS-->
            <div class="col-xs-12 col-sm-12 col-md-6 form-group">
                <asp:Panel ID="PanelDetail" runat="server" Style="background-color: #ffffff; margin-left: auto;"></asp:Panel>
                <h3 style="margin-left: 5px; text-align: center">Users with similar interests</h3>
                <asp:Panel ID="PanelRecommendedFriends" runat="server" Style="background-color: #ffffff; margin-right:auto;"></asp:Panel>
>>>>>>> 51d70a2b95253a161c643f1cbe05527a90414bb2
            </div>
        </div>
    </div>
</asp:Content>
