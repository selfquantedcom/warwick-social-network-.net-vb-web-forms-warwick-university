﻿
Partial Class AdminPosts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Verifies user login 
        Dim sessionVar As String
        Try
            sessionVar = Request.QueryString("sessionVar").ToString()
            If (Session(sessionVar) = sessionVar) Then
            Else
                Response.Redirect("Login.aspx")
            End If
        Catch ex As Exception
            Response.Redirect("Login.aspx")
        End Try

    End Sub

    'Get user's ID from session variable
    Public Function GetUserID() As Object
        Try
            Dim sessionVar As String = Request.QueryString("sessionVar").ToString()
            Return sessionVar
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Protected Sub LinkButtonNewsfeed_Click(sender As Object, e As EventArgs) Handles LinkButtonNewsfeed.Click

    End Sub

    Protected Sub LinkButtonLogout_Click(sender As Object, e As EventArgs) Handles LinkButtonLogout.Click
        'Logout
        Dim sessionVar As String
        sessionVar = Request.QueryString("sessionVar").ToString()
        Session(sessionVar) = ""
        Response.Redirect("Login.aspx")
    End Sub

    'Subpage buttons
    Protected Sub ButtonUsers_Click(sender As Object, e As EventArgs) Handles ButtonUsers.Click
        Dim userID As String = GetUserID()
        Response.Redirect("Admin.aspx?sessionVar=" & userID)
    End Sub

    Protected Sub ButtonPosts_Click(sender As Object, e As EventArgs) Handles ButtonPosts.Click
        Response.Redirect(Request.RawUrl)
    End Sub

End Class
