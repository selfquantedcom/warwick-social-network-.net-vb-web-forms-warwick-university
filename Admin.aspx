﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Admin.aspx.vb" Inherits="Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Warwick Social Network</title>

    <!-- Browser icon -->
    <link rel="shortcut icon" href="Pictures/favicon.ico">

    <!-- Bootstrap -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css" />

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <!-- Top nav -->
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <asp:LinkButton ID="LinkButtonNewsfeed" OnClick="LinkButtonNewsfeed_Click" class="navbar-brand" runat="server">We@Warwick</asp:LinkButton></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <asp:LinkButton ID="LinkButtonLogout" runat="server" OnClick="LinkButtonLogout_Click" title="Logout" class="glyphicon glyphicon-log-out"></asp:LinkButton></li>
                </ul>
            </div>
        </nav>
        <!-- Nav end -->

        <!-- Sub page buttons-->
        <div class="row">
            <div class="form-group col-xs-12 col-sm-12 col-md-8">
                <asp:Button ID="ButtonUsers" class="btn btn-info active col-xs-12 col-sm-12 col-md-2" runat="server" Text="Users" Style="display: inline-block; margin-left: 5px; margin-right: 5px; margin-bottom: 5px;" />
                <asp:Button ID="ButtonPosts" class="btn btn-info col-xs-12 col-sm-12 col-md-2" runat="server" Text="Posts" Style="display: inline-block; margin-left: 5px;" />
            </div>
        </div>

        <!-- Show all users-->
        <div class="container">
            <asp:AccessDataSource ID="AccessDataSourceallUsers" runat="server" DataFile="~/App_Data/1419631.mdb" DeleteCommand="DELETE FROM [users] WHERE [ID] = ?" InsertCommand="INSERT INTO [users] ([ID], [course_id], [user_firstname], [user_lastname], [user_email], [public_privacy], [user_password], [DOB], [picture_url]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)" SelectCommand="SELECT * FROM [users]" UpdateCommand="UPDATE [users] SET [course_id] = ?, [user_firstname] = ?, [user_lastname] = ?, [user_email] = ?, [public_privacy] = ?, [user_password] = ?, [DOB] = ?, [picture_url] = ? WHERE [ID] = ?">
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                    <asp:Parameter Name="course_id" Type="Int32" />
                    <asp:Parameter Name="user_firstname" Type="String" />
                    <asp:Parameter Name="user_lastname" Type="String" />
                    <asp:Parameter Name="user_email" Type="String" />
                    <asp:Parameter Name="public_privacy" Type="Boolean" />
                    <asp:Parameter Name="user_password" Type="String" />
                    <asp:Parameter Name="DOB" Type="DateTime" />
                    <asp:Parameter Name="picture_url" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="course_id" Type="Int32" />
                    <asp:Parameter Name="user_firstname" Type="String" />
                    <asp:Parameter Name="user_lastname" Type="String" />
                    <asp:Parameter Name="user_email" Type="String" />
                    <asp:Parameter Name="public_privacy" Type="Boolean" />
                    <asp:Parameter Name="user_password" Type="String" />
                    <asp:Parameter Name="DOB" Type="DateTime" />
                    <asp:Parameter Name="picture_url" Type="String" />
                    <asp:Parameter Name="ID" Type="Int32" />
                </UpdateParameters>
            </asp:AccessDataSource>
            <asp:GridView ID="GridViewAllUsers" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="AccessDataSourceallUsers">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="course_id" HeaderText="course_id" SortExpression="course_id" />
                    <asp:BoundField DataField="user_firstname" HeaderText="user_firstname" SortExpression="user_firstname" />
                    <asp:BoundField DataField="user_lastname" HeaderText="user_lastname" SortExpression="user_lastname" />
                    <asp:BoundField DataField="user_email" HeaderText="user_email" SortExpression="user_email" />
                    <asp:CheckBoxField DataField="public_privacy" HeaderText="public_privacy" SortExpression="public_privacy" />
                    <asp:BoundField DataField="user_password" HeaderText="user_password" SortExpression="user_password" />
                    <asp:BoundField DataField="DOB" HeaderText="DOB" SortExpression="DOB" />
                    <asp:BoundField DataField="picture_url" HeaderText="picture_url" SortExpression="picture_url" />
                </Columns>
            </asp:GridView>
        </div>

    </form>
</body>
</html>
