﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AdminPosts.aspx.vb" Inherits="AdminPosts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Warwick Social Network</title>

    <!-- Browser icon -->
    <link rel="shortcut icon" href="Pictures/favicon.ico">

    <!-- Bootstrap -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css" />

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <!-- Top nav -->
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <asp:LinkButton ID="LinkButtonNewsfeed" OnClick="LinkButtonNewsfeed_Click" class="navbar-brand" runat="server">We@Warwick</asp:LinkButton></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <asp:LinkButton ID="LinkButtonLogout" runat="server" OnClick="LinkButtonLogout_Click" title="Logout" class="glyphicon glyphicon-log-out"></asp:LinkButton></li>
                </ul>
            </div>
        </nav>

        <!-- Sub page buttons-->
        <div class="row">
            <div class="form-group col-xs-12 col-sm-12 col-md-8">
                <asp:Button ID="ButtonUsers" class="btn btn-info  col-xs-12 col-sm-12 col-md-2" runat="server" Text="Users" Style="display: inline-block; margin-left: 5px; margin-right: 5px; margin-bottom: 5px;" />
                <asp:Button ID="ButtonPosts" class="btn btn-info active col-xs-12 col-sm-12 col-md-2" runat="server" Text="Posts" Style="display: inline-block; margin-left: 5px;" />
            </div>
        </div>
        <!-- Nav end -->

        <!-- Show posts-->
        <div class="container">            
            <asp:AccessDataSource ID="AccessDataSourcePosts" runat="server" DataFile="~/App_Data/1419631.mdb" DeleteCommand="DELETE FROM [posts] WHERE [ID] = ?" InsertCommand="INSERT INTO [posts] ([ID], [post_content], [post_date], [user_id], [post_time]) VALUES (?, ?, ?, ?, ?)" SelectCommand="SELECT * FROM [posts]" UpdateCommand="UPDATE [posts] SET [post_content] = ?, [post_date] = ?, [user_id] = ?, [post_time] = ? WHERE [ID] = ?">
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                    <asp:Parameter Name="post_content" Type="String" />
                    <asp:Parameter Name="post_date" Type="DateTime" />
                    <asp:Parameter Name="user_id" Type="Int32" />
                    <asp:Parameter Name="post_time" Type="DateTime" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="post_content" Type="String" />
                    <asp:Parameter Name="post_date" Type="DateTime" />
                    <asp:Parameter Name="user_id" Type="Int32" />
                    <asp:Parameter Name="post_time" Type="DateTime" />
                    <asp:Parameter Name="ID" Type="Int32" />
                </UpdateParameters>
            </asp:AccessDataSource>
            <asp:GridView ID="GridViewPosts" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="AccessDataSourcePosts">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="post_content" HeaderText="post_content" SortExpression="post_content" />
                    <asp:BoundField DataField="post_date" HeaderText="post_date" SortExpression="post_date" />
                    <asp:BoundField DataField="user_id" HeaderText="user_id" SortExpression="user_id" />
                    <asp:BoundField DataField="post_time" HeaderText="post_time" SortExpression="post_time" />
                </Columns>
            </asp:GridView>

            <!-- Show comments-->
            <asp:AccessDataSource ID="AccessDataSourceComments" runat="server" DataFile="~/App_Data/1419631.mdb" DeleteCommand="DELETE FROM [comment] WHERE [ID] = ?" InsertCommand="INSERT INTO [comment] ([ID], [comment], [comment_date], [post_id], [user_id], [comment_time], [like_id]) VALUES (?, ?, ?, ?, ?, ?, ?)" SelectCommand="SELECT * FROM [comment]" UpdateCommand="UPDATE [comment] SET [comment] = ?, [comment_date] = ?, [post_id] = ?, [user_id] = ?, [comment_time] = ?, [like_id] = ? WHERE [ID] = ?">
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                    <asp:Parameter Name="comment" Type="String" />
                    <asp:Parameter Name="comment_date" Type="DateTime" />
                    <asp:Parameter Name="post_id" Type="Int32" />
                    <asp:Parameter Name="user_id" Type="Int32" />
                    <asp:Parameter Name="comment_time" Type="DateTime" />
                    <asp:Parameter Name="like_id" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="comment" Type="String" />
                    <asp:Parameter Name="comment_date" Type="DateTime" />
                    <asp:Parameter Name="post_id" Type="Int32" />
                    <asp:Parameter Name="user_id" Type="Int32" />
                    <asp:Parameter Name="comment_time" Type="DateTime" />
                    <asp:Parameter Name="like_id" Type="Int32" />
                    <asp:Parameter Name="ID" Type="Int32" />
                </UpdateParameters>
            </asp:AccessDataSource>
            <asp:GridView ID="GridViewComments" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" DataSourceID="AccessDataSourceComments">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="ID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="ID" />
                    <asp:BoundField DataField="comment" HeaderText="comment" SortExpression="comment" />
                    <asp:BoundField DataField="comment_date" HeaderText="comment_date" SortExpression="comment_date" />
                    <asp:BoundField DataField="post_id" HeaderText="post_id" SortExpression="post_id" />
                    <asp:BoundField DataField="user_id" HeaderText="user_id" SortExpression="user_id" />
                    <asp:BoundField DataField="comment_time" HeaderText="comment_time" SortExpression="comment_time" />
                    <asp:BoundField DataField="like_id" HeaderText="like_id" SortExpression="like_id" />
                </Columns>
            </asp:GridView>

        </div>
    </form>
</body>
</html>
